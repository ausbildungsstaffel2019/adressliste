import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdresseingabeComponent } from './adresseingabe.component';

describe('AdresseingabeComponent', () => {
  let component: AdresseingabeComponent;
  let fixture: ComponentFixture<AdresseingabeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdresseingabeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdresseingabeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
