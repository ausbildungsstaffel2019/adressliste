import { Component, OnInit } from '@angular/core';
import { Person } from 'src/commons/person';
import { Transfer } from 'src/commons/transfer';

@Component({
  selector: 'app-adresseingabe',
  templateUrl: './adresseingabe.component.html',
  styleUrls: ['./adresseingabe.component.css']
})
export class AdresseingabeComponent implements OnInit {

  vorname:string;
  nachname:string;

  constructor() { }

  ngOnInit() {
  }

  addUser(){
    let p:Person ={
      vorname: this.vorname,
      name: this.nachname
    }
    Transfer.personen.push(p);
  }
}
