import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdresslisteComponent } from './adressliste.component';

describe('AdresslisteComponent', () => {
  let component: AdresslisteComponent;
  let fixture: ComponentFixture<AdresslisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdresslisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdresslisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
