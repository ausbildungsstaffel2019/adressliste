import { Component, OnInit } from '@angular/core';
import { Person } from 'src/commons/person';
import { Transfer } from 'src/commons/transfer';


@Component({
  selector: 'app-adressliste',
  templateUrl: './adressliste.component.html',
  styleUrls: ['./adressliste.component.css']
})
export class AdresslisteComponent implements OnInit {

  personen = Transfer.personen;

  constructor() { }

  ngOnInit() {
  }


}
